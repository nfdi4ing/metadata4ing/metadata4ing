# m4i Training Materials (WIP)

We are currently working on training materials for the application of m4i.

See the related epic https://git.rwth-aachen.de/groups/nfdi4ing/metadata4ing/-/epics/2.

The issues resulted from a brainstorming at SIG Metadata & Ontologies. See the corresponding [Miro Board](https://miro.com/app/board/uXjVOlwvNt0=/).
