# Using m4i metadata files in RO-Crates
[RO-Crate](https://www.researchobject.org/ro-crate/1.1/) defines a structured and machine-readable format to combine data with their metadata. The [metadata format](https://www.researchobject.org/ro-crate/1.1/metadata.html) bases on schema.org, uses JSON-LD as serialization format and describes a Dataset with its [Data Entities](https://www.researchobject.org/ro-crate/1.1/data-entities.html) together with the [provenance of these entities](https://www.researchobject.org/ro-crate/1.1/provenance.html). The concepts and base classes of RO-Crate metadata are in general compatible to metadata4ing. 

In an RO-Crate there has to be a main metadata file named ro-crate-metadata.json, which refers to the data entities and entails the metadata to describe these entities. 

To transfer a metadata4ing file to a ro-crate-metadata.json file, we have to restructure our file from the process based view of metadata4ing to the dataset based view of an RO-Crate. 

## Step 1: Identify the data entities that will be part of the RO-Crate

In the example of our first steps guide, we have a folder of TIFF files that result from the XRCT-Scan step. We assume, that these files are placed in a folder named output/xrct-files. Let's assume additionally that the reconstruction step results in a model in form of a file object that should also be part of the dataset.

## Step 2: Describe the dataset

An RO-Crate always contains a ro-crate-metadata.json file and a root data entity of type Dataset, which is the container of all files that are part of the RO-Crate. The files itself are identified via their filename.

```
{ "@context": "https://w3id.org/ro/crate/1.1/context", 
  "@graph": [
    {
        "@type": "CreativeWork",
        "@id": "ro-crate-metadata.json",
        "conformsTo": {"@id": "https://w3id.org/ro/crate/1.1"},
        "about": {"@id": "./"}
    },
    
    {
      "@id": "./",
      "@type": "Dataset",
      "hasPart": [
        "output/xrct-data/*.tiff",
	    "model"
      ]
    }
  ]
}
```

For each of these files, we need an entity of type File (schema:MediaObject), that describes each of the files in more details. A directory of other files are defined in RO-Crate as a dataset (schema:Dataset) instead of a file set (cr:FileSet) in m4i. 

```

        {
	        "@type": "Dataset",
	        "@id": "output/xrct-data/",
	        "description": "TIFF files in folder output/image-files"
        },
        {
            "@type": "File",
            "@id": "model",
            "contentSize": "1500"
        }
```




## Step 3: Reuse metadata4ing to describe the provenance of the data entities

To reuse a process documentation in form of a metadata4ing file, we add the m4i-file (in this case named provenance.json) as a data entity to the ro-crate-metadata.json. 


```
{ "@context": "https://w3id.org/ro/crate/1.1/context", 
  "@graph": [
    {
        "@type": "CreativeWork",
        "@id": "ro-crate-metadata.json",
        "conformsTo": {"@id": "https://w3id.org/ro/crate/1.1"},
        "about": {"@id": "./"}
    },
    {
        "@type": "CreativeWork",
        "@id": "provenance.json",
        "conformsTo": {"@id": "https://w3id.org/ro/crate/1.1"},
        "conformsTo": {"@id": "https://w3id.org/nfdi4ing/metadata4ing/1.3.0"},
        "about": {"@id": "./"}
    },
...
```


The whole ro-crate-metadata.json then looks like this:

```
{ "@context": "https://w3id.org/ro/crate/1.1/context", 
  "@graph": [
    {
        "@type": "CreativeWork",
        "@id": "ro-crate-metadata.json",
        "conformsTo": {"@id": "https://w3id.org/ro/crate/1.1"},
        "about": {"@id": "./"}
    },
    {
        "@type": "CreativeWork",
        "@id": "provenance.json",
        "conformsTo": {"@id": "https://w3id.org/ro/crate/1.1"},
        "conformsTo": {"@id": "https://w3id.org/nfdi4ing/metadata4ing/1.3.0"},
        "about": {"@id": "./"}
    },
    {
      "@id": "./",
      "@type": "Dataset",
      "hasPart": [
        "output/xrct-data/*.tiff",
	    "model"
      ]
    },
    {
	    "@type": "Dataset",
	    "@id": "output/xrct-data/",
	    "description": "TIFF files in folder output/image-files"
    },
    {
        "@type": "File",
        "@id": "model",
        "contentSize": "1500"
    }
  ]
}
```

## Step 4: Change context to map m4i to schema.org

To be compatible to RO-Crate, we have to map the central m4i classes and properties in the provenance.json file to the corresponding schema.org classes and properties. 

- m4i:ProcessingStep is mapped to schema:Action
- m4i:Tool is mapped to schema:IndividualProduct
- m4i:NumericalVariable and m4i:TextVariable to schema:PropertyValue

Also the classes for Persons, Organisations and Files are mapped to their corresponding schema.org classes. The whole mapping can be found in [mappings/rocrate-links.ttl](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing/-/raw/1.3.0/mappings/rocrate-links.ttl?ref_type=heads)

To use these mappings to make a metadata4ing file usable in an RO-Crate, you only have to exchange the context reference.

Instead of 

```
{
    "@context": 
        {
            "@import": "https://w3id.org/nfdi4ing/metadata4ing/m4i_context.jsonld",
            "local": "https://local-domain.org/"
        },
    "@graph": [
    ]
}
```

use

```
{
    "@context": 
        {
            "@import": "https://w3id.org/nfdi4ing/metadata4ing/m4i2rocrate_context.jsonld",
            "local": "https://local-domain.org/"
        },
    "@graph": [
    ]
}
```

With this step, all relevant classes and properties of m4i are mapped to the RO-Crate compatible schema.org classes while preserving all additional information basing on m4i.


The whole provenance.json file then looks like this.

```
{
    "@context": 
        {
            "@import": "https://w3id.org/nfdi4ing/metadata4ing/m4i2rocrate_context.jsonld",
            "local": "https://local-domain.org/"
        },
    "@graph": [
        {
            "@id": "local:preparation_0001",
            "@type": "processing step",
            "label": "Sample preparation and parameter definition",
            "has participant": "local:alex",
            "start time": "2022-09-22T10:31:22"
        },
        {
            "@id": "local:xrctscan_0001",
            "@type": "processing step",
            "label": "XRCT-Scan",
            "has employed tool": "local:xray_source_finetec_180.01ctt_0001",
            "realizes method": "local:cone_beam_scanning",
            "investigates": "https://www.wikidata.org/wiki/Q189259",
            "investigatesProperty": "https://www.wikidata.org/wiki/Q256699",
            "has output": "local:output/xrct-data",
        },
        {
	        "@type": "file set",
	        "@id": "output/xrct-data",
	        "includes": "*.tiff",
	        "description": "TIFF files in folder output/image-files"
        },
        {
            "@id": "local:reconstruction_0001",
            "@type": "processing step",
            "label": "Image Processing and Reconstruction",
            "starts with": "local:normalization_0001",
            "has input": "local:xrct_data_0001"
        },
        {
            "@id": "local:postprocessing_0001",
            "@type": "processing step",
            "label": "Post Processing"
        },
        {
            "@id": "local:alex",
            "@type": "person",
            "ORCID Id": "0000-0000-0123-4567",
            "first name": "Alexandra",
            "last name": "Test"
        },
        {
            "@id": "local:xray_source_finetec_180.01ctt_0001",
            "@type": "tool",
            "label": "FineTec FORE 180.01C TT"
        },
        {
            "@id": "local:cone_beam_scanning",
            "@type": "method",
            "label": "Standard cone beam scanning",
            "description": "Standard cone beam scanning of the centered sample",
            "has parameter": 
            {
                "@id": "local:xray_tube_voltage",
                "@type": "numerical variable",
                "label": "X-Ray Tube Voltage",
                "has kind of quantity" : "http://qudt.org/vocab/quantitykind/ElectricPotential", 
                "has numerical value": "140",
    	    	"has unit": "http://qudt.org/vocab/unit/KiloV"
            }
        }
    ]
}
```

While the metadata4ing file is identical for humans and for machines, except for the context, the metadata represents provenance information according to the RO-Crate specification.


## Step 5: Putting it all together

The resulting RO-Crate consists then of two metadata files (ro-crate-metadata.json and provenance.json) and the data files (model and a set of TIFF files in a directory named output/xrct-data). 