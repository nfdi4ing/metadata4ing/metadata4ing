# Building Blocks of an ontology

Metadata4ing models a part of the world by introducing classes and properties. A **class** stands for a group of individuals that all share some common properties. A **subclass** stands for a subset of this group. For example, the class *method* stands for the concept of a scientific method in general; *microscopy* could be a subclass of *method* representing all microscopy methods and *coherent anti-Stokes Raman microscopy* is an **instance** of these classes, i.e. a  **named individual** belonging to the class *method* and the subclass *microscopy*.


![Building blocks of an ontology on the example of tools, instruments and methods](/visualizations/BuildingBlocks.png)


Properties that are attributes of a class are called **datatype properties**. A *method* has for example a *name* as a datatype property. The **range** of a property defines the data type of a property. A *name* can for example have the range *literal*, a *start date* the range *date*. A **domain** of a property is the class that has this property. 

Relations between classes that can be used to interlink instances of these classes are called **object properties**. A *tool* *implements* a *method*, so "*implements*" is an object property with the domain *tool* and the range *method*.

[Back to first-steps guide](first-steps-guide.md)
