# Adding Substeps

Individual [processing steps](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#ProcessingStep) can also contain sub-steps. For example, the step Image Processing and Reconstruction contains the steps Normalization, Correction, Filters, Application of the Reconstruction Algorithm and Evaluation of the reconstructed dataset. The individual substeps are also created as processing steps and then linked to the parent step via a [part-of relationship](https://w3id.org/nfdi4ing/metadata4ing#http://purl.obolibrary.org/obo/BFO_0000050).

    ```
    {
  	"@graph": [
	    {
		"@id":"local:preparation_0001",
		"@type": "processing step",
		"label": "Sample preparation and parameter definition"
	    },
	    {
	    "@id":"local:xrctscan_0001",
	    "@type": "processing step",
	    "label": "XRCT-Scan"
	    },
	    {
	    "@id":"local:reconstruction_0001",
	    "@type": "processing step",
	    "label": "Image Processing and Reconstruction",
            "starts with": "local:normalization_0001"
	    },
	    {
	    "@id":"local:postprocessing_0001",
	    "@type": "processing step",
	    "label": "Post Processing"
	    },
	    {
	    "@id":"local:normalization_0001",
	    "@type": "processing step",
	    "label": "Normalization",
        "usage instruction": "Normalization and Linearization of the projection images. Transforming the data into sinograms",
          "part of": "local:reconstruction_0001"
	    },
	    {
	    "@id":"local:correction_0001",
	    "@type": "processing step",
	    "label": "Correction",
        "usage instruction": "Correction of geometrical system misalignments",
          "part of": "local:reconstruction_0001"
	    },
	    {
	    "@id":"local:filters_0001",
	    "@type": "processing step",
	    "label": "Filters",
	    "usage instruction": "Filter selection and application",
          "part of": "local:reconstruction_0001"
	    },
	    {
	    "@id": "local:reconstruction_alg_0002",
	    "@type": "processing step",
	    "label": "Algorithm application",
	    "usage instruction": "Applying reconstruction algorithms",
	    "part of": "local:reconstruction_0001"
	    },
	    {
	    "@id":"local:evaluation_0001",
	    "@type": "processing step",
	    "label": "Evaluation",
          "usage instruction": "Evaluation of the reconstructed dataset",
          "part of": "local:reconstruction_0001"
	    }
      ]
}
    ```


[Back to first-steps guide](first-steps-guide.md)