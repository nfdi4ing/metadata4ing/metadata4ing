# How to Use Metadata4Ing - First Steps Tutorial

[[_TOC_]]

## Objective of this Guide
This guide demonstrates how to model metadata for research processes and resulting datasets in engineering with the ontology [Metadata4Ing 1.2.1](http://w3id.org/nfdi4ing/metadata4ing/1.2.1). The guide will enable you to create machine-understandable metadata for your own research data, following Semantic Web standards like [RDF](http://www.w3.org/TR/rdf11-primer/), [RDFS](http://www.w3.org/TR/rdf-schema/), [OWL](http://www.w3.org/TR/owl2-primer/), and [JSON-LD](https://www.w3.org/TR/json-ld/). A basic introduction to these formats is offered by the [Carpentries guide on "Scientific Metadata"](https://carpentries-incubator.github.io/scientific-metadata/structure-schema.html#json).

## Target Group

- application developers
- research software engineers
- data stewards
- tech-savvy domain experts

## Metadata4Ing at a Glance

Metadata4Ing (m4i) is an ontology for a process-based description of research activities and their results, focusing on the provenance of both data and material objects. m4i is intended as a general process model applicable to many disciplines and focuses on general concepts like in- and output, employed methods and tools as well as the investigated entity, reusing terms of existing ontologies where possible. If you want to read more about the main building blocks of ontologies, see [Building Blocks](building-blocks.md).

## Why use m4i to describe your research data?

Metadata contains structured information for a context-related description of data and is, so to speak, data about data. Metadata is required to locate, manage, and use data, not only when data is published, but also in active everyday research. In this respect, it is important that all the information, which is required for finding and understanding the data, is expressed using a common and consistent language that consists of unambiguous well-documented terms. This approach is a prerequisite for [FAIR (meta)data](https://www.go-fair.org/fair-principles/), especially for their [interoperability](https://www.go-fair.org/fair-principles/i1-metadata-use-formal-accessible-shared-broadly-applicable-language-knowledge-representation/). 

m4i provides a general process-based model that allows a flexible description of research activities and their results, focusing on the provenance of both data and material objects. m4i offers a selection of general concepts like processing steps, in- and output, employed methods and tools, that allows modelling information about research processes and results in a structured, consistent and machine-actionable way.

One of the main benefits of using m4i is that the resulting description is highly interoperable and allows integration of data from very different scientific disciplines into a single knowledge graph. In addition, m4i heavily reuses concepts from well-known general or top-level ontologies, e.g. [Basic Formal Ontology (BFO)](https://basic-formal-ontology.org/), [Data Catalog Vocabulary (DCAT)](https://www.w3.org/TR/vocab-dcat-2/) or [PROV Ontology (PROV-O)](https://www.w3.org/TR/prov-o/), which seamlessly embeds information modelled in m4i in larger contexts.

By documenting your research data with m4i, you not only fulfill the requirements of [good scientific practice](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/gwp/), but can also make use of consistent metadata when searching for, analyzing or otherwise using your data, and also benefit during collaborative work. You can store RDF metadata as [JSON-LD](#describe-research-processes-with-JSON-LD-Files). This format offers semantically enriched information that is understandable by humans and machines. In addition, having a machine-actionable documentation of your data available also facilitates publishing or archiving your data in data repositories in a citable way.

## How to model a Research Process and its Results with m4i?

This guide demonstrates how to model a research process and its results with m4i using the example of a material examination by a micro X-ray computed tomograph.

In this example (thanks to Matthias Ruf of the working group of Holger Steeb at the University of Stuttgart), a sample of a material (in this case asphalt concrete) is examined with an XCRT-scanner. The whole process consists of four steps:
1. preparation and positioning of the sample, and configuration of the parameters
2. data generation in form of the XRCT scan,
3. image processing with the help of reconstruction algorithms and
4. post processing of the data.

The experimental setup consists of a holder for precise positioning of the sample, an X-Ray source and a detector and is described in detail in [doi:10.1063/5.0019541](https://doi.org/10.1063/5.0019541).

The sample is then scanned while being automatically rotated resulting in a high number of TIFF images, the projection images or radiograms. These images are then used as an input for a software (Octopus Reconstruction), that reconstructs the slices of the material.

A resulting dataset can be found on [doi:10.18419/darus-639](https://doi.org/10.18419/darus-639).

### Describe Research Processes with JSON-LD-Files

To describe (a simplified version of) this example with m4i and generate a JSON-LD file, we start with an empty text file. Alternatively, you can use a graphical tool like Protégé to create different serializations of your process like Turtle or JSON-LD, [cf. our guide "Using Protégé (WIP)"](/training/using-protege/using-protege.md).

#### Import Context

By adding the following line to our JSON-LD file, we give the metadata the semantic context that clearly defines what exactly is meant by the information in the file. Keywords with special meaning are identified in JSON-LD files using an @ sign in the key.

```json
{
    "@context": 
    {
        "@import": "https://w3id.org/nfdi4ing/metadata4ing/m4i_context.jsonld"
    }
}
```

If you compare the [documentation of m4i](http://w3id.org/nfdi4ing/metadata4ing) with the context file, you will see that the context file adds the label of a class, property or instance as a shortcut for the ID (IRI). So you can always use the label to refer to the ID of a concept. 

#### Add Metadata

Since we want to document multiple different elements - processing steps, people, tools, methods, and objects of investigation - within our metadata, we add a graph, i.e. a list of elements, to our JSON-LD file, into which we will later add the different elements. 

```json
{
    "@context": 
    {
        "@import": "https://w3id.org/nfdi4ing/metadata4ing/m4i_context.jsonld"
    },
    "@graph": [ ]
}
```

#### Define Processing Steps
In the next step we can add the different  [Processing Steps](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#ProcessingStep): Preparation, Scan, Image Reconstruction and Post-Processing.

```json
{
    "@context": 
    {
        "@import": "https://w3id.org/nfdi4ing/metadata4ing/m4i_context.jsonld",
        "local": "https://local-domain.org/"
    },
    "@graph": [
        {
            "@id":"local:preparation_0001",
            "@type": "processing step",
            "label": "Sample preparation and parameter definition"
        },
        {
            "@id":"local:xrctscan_0001",
            "@type": "processing step",
            "label": "XRCT-Scan"
        },
        {
            "@id":"local:reconstruction_0001",
            "@type": "processing step",
            "label": "Image Processing and Reconstruction"
        },
        {
            "@id":"local:postprocessing_0001",
            "@type": "processing step",
            "label": "Post Processing"
        }
    ]
}
```

Here we define four specific processing steps as instances of the [ProcessingStep](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#ProcessingStep) class by using the @type attribute. With the @id attribute, we assign a unique identifier to this instance that we can refer to later. You can use whatever unique string you like as an id. We define a local namespace to ensure that the chosen ids have to be unique only in our local context. Just replace "https://local-domain.org/" with something unique for your research, e.g. the URL of your research group. Ideally, you use already globally unique ids like [UUID](https://de.wikipedia.org/wiki/Universally_Unique_Identifier)s. 

Optionally, you can also [add substeps](adding-substeps.md) to the process with the help of the [part of](https://w3id.org/nfdi4ing/metadata4ing#http://purl.obolibrary.org/obo/BFO_0000050) relation.  

From here on, for reasons of compactness, we will only consider the graph and hide the lines for importing the context file.

#### Add Actors

As a next step, we want to specify that Alex prepared the sample on 2022-09-22 at 10:31. For this we add Alex as a new element of the type person and link her ID with the processing step. With the help of [start time](https://w3id.org/nfdi4ing/metadata4ing#https://schema.org/startTime) we can additionally specify a date with time. 

```json
...
"@graph": [
    {
        "@id":"local:preparation_0001",
        "@type": "processing step",
        "label": "Sample preparation and parameter definition",
        "has participant": "local:alex",
        "start time": "2022-09-22T10:31:22"
    },
    ...
    {
        "@id": "local:alex",
        "@type": "person",
        "ORCID Id": "0000-0000-0123-4567",
        "first name": "Alexandra",
        "last name": "Test"
    }
]
```

#### Add Tools and Methods to [Processing Steps](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#ProcessingStep)

We now proceed in the same way to add the X-ray source as a tool and the method used to the scan step.

First, we create the "FineTec FORE 180.01C TT" device as a [tool](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#Tool) and link it to the processing step via [has employed tool](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#hasEmployedTool).

```json
...
"@graph": [
    {
        "@id":"local:xrctscan_0001",
        "@type": "processing step",
        "label": "XRCT-Scan",
        "has employed tool": "local:xray_source_finetec_180.01ctt_0001"
    },
    ...
    {
        "@id": "local:xray_source_finetec_180.01ctt_0001",
        "@type": "tool",
        "label": "FineTec FORE 180.01C TT"
    }
]
```

In order to additionally include the method used, we create "Standard Cone Beam Scanning" as a method and link the [method](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#Method) to the [processing step](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#ProcessingStep) via [realizes method](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#realizesMethod).

```json
...
"@graph": [
    {
    "@id":"local:xrctscan_0001",
    "@type": "processing step",
    "label": "XRCT-Scan",
     "has employed tool": "local:xray_source_finetec_180.01ctt_0001",
     "realizes method": "local:cone_beam_scanning"
    },
    ...
    {
        "@id": "local:cone_beam_scanning",
        "@type": "method",
        "label": "Standard cone beam scanning",
        "description": "Standard cone beam scanning of the centered sample"
    }
]
```

#### Add Object of Research

Let us now add that the XRCT scan examines the [permeability](https://www.wikidata.org/wiki/Q256699) of [asphalt](https://www.wikidata.org/wiki/Q189259).

```json
"@graph": [
    ...
{
    "@id":"local:xrctscan_0001",
    "@type": "processing step",
    "label": "XRCT-Scan",
    "has employed tool": "local:xray_source_finetec_180.01ctt_0001",
    "realizes method": "local:cone_beam_scanning",
    "investigates": "https://www.wikidata.org/wiki/Q189259",
    "investigatesProperty": "https://www.wikidata.org/wiki/Q256699"
}
```

Using [investigates](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#investigates), and [investigates property](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#investigatesProperty), we link the Wikidata URIs for the concepts asphalt and permeability to the [processing step](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#ProcessingStep). The Wikidata concepts are not defined within m4i. We can use concepts from any other ontology to combine them with m4i.


#### Add Parameters

Parameters can be assigned to a [method](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#Method), a [tool](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#Tool) or directly to a [processing step](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#ProcessingStep). 

Let's assume, that we want to add an X-ray tube voltage of 140 kV as a parameter to the method "Standard cone beam scanning" of the XRCT processing step. First, we use [has parameter](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#hasParameter) to add the X-ray tube voltage as a numerical variable. We can optionally define the quantity type of the variable "Electric Potential" by the corresponding [term of the QUDT vocabulary](https://qudt.org/vocab/quantitykind/ElectricPotential) via [has kind of quantity](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#hasKindOfQuantity).

```json
"@graph": [
    ...
    {
        "@id": "local:cone_beam_scanning",
        "@type": "method",
        "label": "Standard cone beam scanning",
        "description": "Standard cone beam scanning of the centered sample",
        "has parameter": 
        {
            "@id": "local:xray_tube_voltage",
            "@type": "numerical variable",
            "label": "X-Ray Tube Voltage",
            "has kind of quantity" : "http://qudt.org/vocab/quantitykind/ElectricPotential"
        }
    }
    ...
]
```

As a next step, we assign the value 140 to this parameter via the property [has numerical value](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#hasNumericValue). The unit kV is represented by the [corresponding term of the QUDT vocabulary](https://qudt.org/vocab/unit/KiloV) and added via [has unit](http://w3id.org/nfdi4ing/metadata4ing/index.html#http://w3id.org/nfdi4ing/metadata4ing#hasUnit).

```json
"@graph": [
    ...
    {
        "@id": "local:cone_beam_scanning",
        "@type": "method",
        "label": "Standard cone beam scanning",
        "description": "Standard cone beam scanning of the centered sample",
        "has parameter": 
        {
            "@id": "local:xray_tube_voltage",
            "@type": "numerical variable",
            "label": "X-Ray Tube Voltage",
            "has kind of quantity" : "http://qudt.org/vocab/quantitykind/ElectricPotential", 
            "has numerical value": "140",
	    "has unit": "http://qudt.org/vocab/unit/KiloV"
        }
    }
]
```

Instead of assigning a specific value to a numerical variable, we could also assign a value range by using the properties [has minimal value](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#hasMinimalValue) and [has maximum value](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#hasMaximumValue) as an alternative of [has numerical value](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#hasNumericValue). 
For variables that have no numerical values but text based values, we could also assign a string value to a variable by using the type [text variable](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#TextVariable) instead of a [numerical variable](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#NumericalVariable) and the property [has string value](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#hasStringValue) instead of [has numerical value](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#hasNumericValue). See [the documentation](https://w3id.org/nfdi4ing/metadata4ing#3-9-specifying-parameters-by-variables) for more details about specifying parameters. 

#### Add Input und Output to a [Processing Step](https://w3id.org/nfdi4ing/metadata4ing#http://w3id.org/nfdi4ing/metadata4ing#ProcessingStep)

If we now want to specify that the XRCT scan outputs a set of multiple images, which builds the input of the reconstruction step. We can specify this via the [has output](https://w3id.org/nfdi4ing/metadata4ing#http://purl.obolibrary.org/obo/RO_0002234) and [has input](https://w3id.org/nfdi4ing/metadata4ing#http://purl.obolibrary.org/obo/RO_0002233) properties. The collection of all images is a [(crossant:) file set](https://w3id.org/nfdi4ing/metadata4ing#http://mlcommons.org/croissant/FileSet), representing a folder of image files. Alternatively, the individual images could be modeled as [(crossant:) file object](https://w3id.org/nfdi4ing/metadata4ing#http://mlcommons.org/croissant/FileObject)s.


```json
"@graph": [
    ...
  {
    "@id": "local:xrctscan_0001",
    "@type": "processing step",
    "label": "XRCT-Scan",
    "has employed tool": "local:xray_source_finetec_180.01ctt_0001",
    "realizes method": "local:cone_beam_scanning",
    "investigates": "https://www.wikidata.org/wiki/Q189259",
    "investigatesProperty": "https://www.wikidata.org/wiki/Q256699",
    "has output": "local:output/xrct-data",
  },
  {
	"@type": "file set",
	"@id": "local:output/xrct-data",
	"includes": "*.tiff",
	"description": "TIFF files in folder output/image-files"
  }
}
...
{
    "@id": "local:reconstruction_0001",
    "@type": "processing step",
    "label": "Image Processing and Reconstruction",
    "has input": "local:xrct_data_0001"
}
```

The full JSON-LD file will look like this:

```
{
    "@context": 
        {
            "@import": "https://w3id.org/nfdi4ing/metadata4ing/m4i_context.jsonld",
            "local": "https://local-domain.org/"
        },
    "@graph": [
        {
            "@id": "local:preparation_0001",
            "@type": "processing step",
            "label": "Sample preparation and parameter definition",
            "has participant": "local:alex",
            "start time": "2022-09-22T10:31:22"
        },
        {
            "@id": "local:xrctscan_0001",
            "@type": "processing step",
            "label": "XRCT-Scan",
            "has employed tool": "local:xray_source_finetec_180.01ctt_0001",
            "realizes method": "local:cone_beam_scanning",
            "investigates": "https://www.wikidata.org/wiki/Q189259",
            "investigatesProperty": "https://www.wikidata.org/wiki/Q256699",
	    "has output": "local:output/xrct-data"
        },
        {
            "@id": "local:reconstruction_0001",
            "@type": "processing step",
            "label": "Image Processing and Reconstruction",
            "has input": "local:xrct_data_0001"
        },
        {
            "@id": "local:postprocessing_0001",
            "@type": "processing step",
            "label": "Post Processing"
        },
        {
            "@id": "local:alex",
            "@type": "person",
            "ORCID Id": "0000-0000-0123-4567",
            "first name": "Alexandra",
            "last name": "Test"
        },
        {
            "@id": "local:xray_source_finetec_180.01ctt_0001",
            "@type": "tool",
            "label": "FineTec FORE 180.01C TT"
        },
        {
            "@id": "local:cone_beam_scanning",
            "@type": "method",
            "label": "Standard cone beam scanning",
            "description": "Standard cone beam scanning of the centered sample",
            "has parameter": 
            {
                "@id": "local:xray_tube_voltage",
                "@type": "numerical variable",
                "label": "X-Ray Tube Voltage",
                "has kind of quantity" : "http://qudt.org/vocab/quantitykind/ElectricPotential", 
                "has numerical value": "140",
    	    	"has unit": "http://qudt.org/vocab/unit/KiloV"
            }
        },
  	{
		"@type": "file set",
                "@id": "local:output/xrct-data",
  		"includes": "*.tiff",
		"description": "TIFF files in folder output/image-files"
  	}
    ]
}
```

## What to do with the Metadata File?

In this guide, we created a machine-readable JSON-LD file that documents a specific research process with its processing steps, actors, and the used methods and tools. But how to proceed with this file?
* **Add the metadata file to your data**: Without any other tools, you can just add the metadata file (e.g. in a .metadata folder) to your research data or code, upload it to a data repository or a data archive together with the data files. 
*  **Use the metadata file in your own scripts and software**: A JSON-LD file can be read like normal JSON-Files. So you can use the inclosed information in your scripts and tools that you use in your research process.
* **Upload the metadata to a data repository**: Depending on the functionalities of a data repository, the metadata contained can be automatically transferred to create or update a dataset. 
* **Integrate the metadata in an RO-Crate Archive**: A [Research Object Crate](https://www.researchobject.org/ro-crate/1.1/introduction.html) (RO-Crate) is a method to aggregate and describe research data with metadata in form of a zip archive that entails data together with metadata. The metadata in an RO-Crate is provided in form of a JSON-LD file based on schema.org classes. To make m4i metadata compliant with the RO Crate metadata specification, just replace the standard m4i context file ([m4i_context.jsonld](https://w3id.org/nfdi4ing/metadata4ing/m4i_context.jsonld)) by a m4i2rocrate context file ([m4i2rocrate_context.jsonld](https://w3id.org/nfdi4ing/metadata4ing/m4i2rocrate_context.jsonld)) that maps the m4i classes to the corresponding schema.org classes (see [Use m4i in RO-Crate](/training/use-in-rocrate.md) for more information).

## Where to find information about m4i? 

An overview of m4i classes and properties is available at our [ontology documentation](https://metadata4ing.org):
- [Section 1](https://w3id.org/nfdi4ing/metadata4ing#intro) gives a short introduction into the ontology's purpose,
- [Section 2](https://w3id.org/nfdi4ing/metadata4ing#overv) provides a list of all classes, properties and individuals of m4i,
- [Section 3](https://w3id.org/nfdi4ing/metadata4ing#desc) illustrates the use of the classes and gives a detailed overview of m4i's scope,
- [Section 4](https://w3id.org/nfdi4ing/metadata4ing#crossreference) explains each term of the m4i ontology - this section is autogenerated from the ontology.

The ontology code is developed at [m4i's GitLab repository](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing) where also its [releases](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing/-/releases) are published. There you can also find examples for the usage of m4i, make a proposal for further development in the [form of an issue](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing/-/issues) or get information [how to contribute](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing/-/blob/master/docs/editors_guide.md) to m4i. 

## Additional resources and further reading
We recommend these resources for additional information and testing of JSON-LD files:

### JSON-LD 1.1 - A JSON-based Serialization for Linked Data by the W3C
JSON-LD is a standard issued by the World Wide Web Consortium (W3C). You can find the latest documentation at the [W3C recommendation on JSON-LD 1.1](https://www.w3.org/TR/json-ld11/). There is also a first draft for a [YAML-LD specification](https://json-ld.github.io/yaml-ld/spec/) that could perhaps also be used as a serialization format for m4i in the future.

### JSON-LD.org
JSON-LD.org is a great address to learn a lot more about JSON-LD, e.g. test and validate your JSON-LD files or find more information about JSON-LD in development environments like JavaScript, Python, and others. Some useful resources are:
- the [JSON-LD Playground](https://json-ld.org/playground/);
- the [Developers' Guide](https://json-ld.org/#developers);
- [introductory material, articles and presentations](https://json-ld.org/learn.html).

### _FAIR and Knowledge Graphs_ in the FAIRCookbok by Elixir
The [FAIR Cookbook](https://faircookbook.elixir-europe.org/content/home.html) is generally a very good resource to learn more about technical and methodical workflows relating to FAIR data management. We especially recommend their guidance recipe on [FAIR and Knowledge Graphs](https://w3id.org/faircookbook/FCB070).

## License and Citation

This guide is licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/). See also the [full license text](https://creativecommons.org/licenses/by/4.0/legalcode.txt).

### How to Cite

Iglezakis, D., Arndt, S., Lanza, G., Terzijska, D., Leimer, S., Fuhrmans, M. (2022). _How to Use Metadata4Ing - First Steps Tutorial_. NFDI4Ing. <https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing/-/blob/master/training/first-steps-guide.md> 