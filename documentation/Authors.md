|Name                  |Activity   |[ORCID]            |Affiliation (acronym)                        |[ROR]    |[ISNI]          |
|----------------------|-----------|-------------------|---------------------------------------------|---------|----------------|
|Dorothea Iglezakis    |1.0.0-     |0000-0002-8524-0569|Universität Stuttgart                        |04vnq7t77|0000000419369713|
|Giacomo Lanza         |1.0.0-     |0000-0002-2239-3955|Physikalisch-Technische Bundesanstalt (PTB)  |05r3f7h03|0000000121861887|
|Susanne Arndt         |1.0.0-     |0000-0002-1019-9151|Technische Informationsbibliothek (TIB)      |04aj4c181|0000000121746694|
|Marc Fuhrmans         |1.0.0-     |0000-0002-9826-018X|Technische Universität Darmstadt             |05n911h24|0000000109401669|
|Džulia Terzijska      |1.0.0-     |0000-0002-1698-6826|Technische Universität Braunschweig (TUBS)   |010nsgg66|0000000110900254|
|Sophia Leimer         |1.0.0-     |0000-0001-6272-204X|Universität Duisburg-Essen                   |04mz5ra38|0000000121875445|
|Johannes Theissen-Lipp|1.0.0-     |0000-0002-2639-1949|RWTH Aachen University                       |04xfq0f34|0000000121811804|
|Stephan Hachinger     |1.0.0-     |0000-0001-8341-1478|Leibniz Supercomputing Centre (LRZ)          |05558nw16|0000000123369749|
|Johannes Munke        |1.0.0-     |0000-0002-5031-9170|Leibniz Supercomputing Centre (LRZ)          |05558nw16|0000000123369749|
|Benjamin Farnbacher   |1.0.0-     |0000-0002-1489-6501|Technische Universität München (TUM)         |02kkvpp62|0000000123222966|
|Vasiliki Sdralia      |           |0000-0002-7213-5110|Technische Universität München (TUM)         |02kkvpp62|0000000123222966|
|Alexander Wellmann    |           |0000-0002-9033-5538|Leibniz Supercomputing Centre (LRZ)          |05558nw16|0000000123369749|
|Ashish Karmacharya    |1.0.0-1.2.0|0000-0003-2822-7648|Technische Universität Darmstadt             |05n911h24|0000000109401669|
|Cord Wiljes           |1.0.0-1.2.0|0000-0003-2528-5391|Nationale Forschungsdateninfrastruktur (NFDI)|05qj6w324|                |
|Nils Hoppe            |1.0.0-1.2.0|0000-0003-0580-9717|Technische Universität München (TUM)         |02kkvpp62|0000000123222966|
|Johanna Hickmann      |1.0.0-1.2.0|0000-0002-7535-8344|Physikalisch-Technische Bundesanstalt (PTB)  |05r3f7h03|0000000121861887|
|Jürgen Windeck        |1.0.0-1.2.0|0000-0003-1909-4353|Technische Universität Darmstadt             |05n911h24|0000000109401669|
|Martin Thomas Horsch  |1.0.0-1.2.0|0000-0002-9464-6739|Norges miljø- og biovitenskapelige universitet (NMBU)|04a1mvv97|000000040607975X|

[ROR]:   <https://ror.org/search>
[ISNI]:  <https://isni.oclc.org/>
[ORCID]: <https://orcid.org/>
