# Contributors' Guide

Thank you for your interest in contributing to metadata4ing.

There are several ways to contribute to metadata4ing:
- propose a class or property
- add a label or definition
- formulate a competency question
- provide an application example
- improve the documentation
- improve our workflow and its documentation

This guide is meant to help you make a contribution. If it does **not**, please let us know.
## 1 Comment and discuss

If you have an idea for improvement, a comment to the ontology or want to discuss anything around metadata4ing, you can either join our mailinglist (metadata4ing@nfdi4ing.de), attend one of the biweekly meetings of the working group or [create a new issue](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing/-/issues). Please check first, if there is already a corresponding issue.

You are also very welcome to participate in the discussion of existing issues.

## 2 Competency Questions

In order to capture the requirements to the ontology, we work with so-called competency questions. Just imagine, there is already a corpus of well defined and indexed research results that would be interesting for your research. What questions would you like to ask to this corpus?

We collect the competency questions [in form of issues](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/kompetenzfragen/-/issues). Feel free to add new competency question and discuss or translate existing competency questions. See the [README](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/kompetenzfragen/-/tree/master) for more information.

We use these questions to investigate which concepts, relations and properties should be part of metadata4ing.

## 3 Code, workflow and documentation

If you would like to change the ontology, the documentation, add an example or change any other file in the repository, please follow these steps:

1. Create a new issue describing the problem or the goal you would like to achieve. The issue is the right place to discuss open questions and alternative solutions with the group.
2. Visit your issue and click on the arrow next to the button "Create merge request". This will open a little dialog. Here choose:
   - Create merge request and branch
   - set `Source (branch or tag)` to `develop`
   - These options will create a branch and a draft merge request which are both linked to your issue.
3. Edit files on the branch, either locally or online, and commit your suggestions. Do not forget to also describe your changes in the documentation.
4. Have your suggestions discussed online or during the Metadata4ing two-weekly meetings.
5. When your suggestions are complete and well-accepted mark your merge request as ready.
6. Your suggestions will be merged.

### 3.1 Branching structure

> TODO: We need to clarify how to enforce this branching structure and change the workflow description accordingly.

The GitLab repository of Metadata4Ing works with several branches.

- `master`: This is the branch on which we will publish our releases.
- `develop`: This is the branch where the latest accepted changes are published that will be part of the next release.
- `features`: There can be several feature branches. These should refer to a well-defined and well-understood issue and a corresponding merge request. The changes on these branches are work in progress and should only be merged to develop when the addition has been approved by the working group.

**(!)** If you want to work with others on the same branch, make sure to pull from remote every time you would like to proceed. Solve conflicts locally to avoid them in merge requests.

### 3.2 Change reviews

When a user marks their draft as finished, the m4i team needs to review the suggested changes.

> TODO: describe responsibilities! who does the reviews? How many people need to agree on changes? What happens if a merge request does not get any response?

- if you agree with the changes: give a thumbs up or approve merge request
- if you disagree with changes
  - give at least a thumbs down
  - provide your criticism as a [comment in the merge request diff](https://git.rwth-aachen.de/help/user/discussions/index.md#add-a-comment-to-a-merge-request-diff)
  - make a [suggestion to change the new code in the merge request](https://git.rwth-aachen.de/help/user/project/merge_requests/reviews/suggestions.md)

### 3.3 Acceptance criteria

> TODO We could define criteria for acceptance. Here are some suggestions which were already mentioned in issues:

- changes to ontology:
  - ontology changes are syntactically correct (ontology loads with Protégé)
  - ontology is coherent and consistent
  - ontology terms are annotated with English labels and definitions
  - all changes are sufficiently addressed in dependant resources:
    - [documentation files](/documentation) (textual descriptions, visualisations)
    - the [first steps guide](/training/first-steps-guide.md)
    - [context file](/m4i_context.jsonld) needs to be updated via [its script](/ci/generate_context_file.py)
    - [application examples](/application_examples)
  - changes are valid from the perspective of the engineering community

### 3.4 Merging
When overall agreement is reached, the changes need to be merged. As the person doing the merge, please make sure to

- squash commits
- delete the source branch (if no longer needed)

## 4 Release Management

All m4i releases can be found at https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing/-/releases. To create a relase the following steps are necessary.

### Preparing a release
- All ontology metadata (authors, version number, modification date, imports) are updated in all relevant files:
  - [Widoco configuration file](/config.properties)
  - [ontology source file](/metadata4ing.ttl)
  - [tabular list of authors](/documentation/Authors.md)
  - [citation template for Zenodo](/CITATION.cff)
- The customized parts of the [documentation](/documentation) should be checked for consistency with the ontology. Do not forget to check the info graphics!
- The [context file](/m4i_context.jsonld) needs to be updated via [its script](/ci/generate_context_file.py).
- Other dependent resources need to be updated:
  - [First steps guide](/training/first-steps-guide.md)
  - [application examples](/application_examples)
- Prepare a release note by going through the changes since last release (cf. the [project graph](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing/-/network/develop)).

### Making the release
When all changes are final, 
- the development branch can be merged into the master branch (do NOT squash commits)
- a tag can be created from master branch
- a release with that tag can be created

The release should name and link to the following release artifacts on the respective tag:
- the first steps guide from the respective tag
- the respective version of the documentation
- the Turtle serialization generated by Widoco online at the respective version of the documentation

### After the release

After the release, a couple of checks need to be done and other resources need to be updated.

- A stable version of documentation needs to be generated, a release folder needs to be created in [/documentation/stable-versions](/documentation/stable-versions) and a folder needs to be added in the CI-pipeline (gen-pages).
- It should be checked whether the documentation is error-free and serves the correct version.
- It should be checked whether all ontology serializations return status 200 and that all w3id settings are working as expected.
The following registries need to be updated:
- Doro: Zenodo archive (https://zenodo.org/record/5957104)
- Susanne: Terminology service (https://terminology.tib.eu/ts/ontologies/m4i)
- Doro: FairSharing (https://fairsharing.org/FAIRsharing.f8b3ec)
- Susanne: Linked Open Vocabularies (https://lov.linkeddata.es/dataset/lov/vocabs/m4i)
- Susanne: Bartoc (https://bartoc.org/en/node/20402)
- Marc: Zazuko (https://prefix.zazuko.com/prefix/m4i:)
