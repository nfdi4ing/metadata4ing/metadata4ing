# Automatic documentation using GitLab pipeline

Automatic documentation using Gitlab pipeline and [Widoco](https://github.com/dgarijo/Widoco).

The pipeline is currently configured to run on every commit.
If otherwise desired, it is also possible to start it on specific versions of the ontology using so-called GitLab tags, or to start it manually.
The pipeline consists of the following steps:

- Check of the graph integrity by using a docker image (python:3.10.11) with RDFlib
- Generation of the JSON-LD context file

- For commits in the develop or master branch: Generation of the documentation by using a Docker image (ruby:2.7) with Java environment:
  - Download Widoco incl. required packages
  - Run Widoco, which generates a website for documentation including visualization with WebVOWL, OOPS error scan and different serializations of the ontology in the chosen configuration
  - Replacing placeholder texts in sections 1. Introduction, 3. Description, 5. References and 6. Acknowledgements by our documentation inputs in folder /documentation.
  - Injection of javascript code for hiding usused elements (hideUnusedElements.js)
  - Small correction of the generated HTML website, because a link is sometimes generated incorrectly
  
  - Provision of the documentation as a public HTML page, generated automatically out of the GitLab repository:
    - A documentation page for the current stable version/release (master branch) --> [https://nfdi4ing.pages.rwth-aachen.de/metadata4ing/metadata4ing/index.html](https://nfdi4ing.pages.rwth-aachen.de/metadata4ing/metadata4ing/index.html), that is also the landing page for [metadata4ing.org](https://metadata4ing.org) and [w3id.org/nfdi4ing/metadata4ing/](https://w3id.org/nfdi4ing/metadata4ing/).
    - A documentation page for each already published stable release `x.x.x` --> `https://nfdi4ing.pages.rwth-aachen.de/metadata4ing/metadata4ing/x.x.x/index.html`, e.g. 1.2.0 --> [https://nfdi4ing.pages.rwth-aachen.de/metadata4ing/metadata4ing/1.2.0/index.html](https://nfdi4ing.pages.rwth-aachen.de/metadata4ing/metadata4ing/1.2.0/index.html).
    - A documentation page for the work in progress (develop branch) --> [https://nfdi4ing.pages.rwth-aachen.de/metadata4ing/metadata4ing/develop/index.html](https://nfdi4ing.pages.rwth-aachen.de/metadata4ing/metadata4ing/develop/index.html).

  
- If a new release is finished, the stable version of the documentation of this release has to be copied into the documentation/stable-versions/ folder and added into the CI pipeline script that copies the stable versions into the public folder for the GitLab pages. 
