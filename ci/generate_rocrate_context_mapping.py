from rdflib import Graph
from rdflib.namespace import NamespaceManager
from rdflib.term import URIRef
from pathlib import Path

m4i = Path(__file__).parent.parent/'metadata4ing.ttl'
mapping = Path(__file__).parent.parent/'mappings/rocrate-links.ttl'
context_file = Path(__file__).parent.parent/'m4i_context_generated.jsonld'
ro_crate_context_file = Path(__file__).parent.parent/'m4i2rocrate_context.jsonld'
predefined_entities = {}
predefined_entities["label"] = "rdfs:label"

g = Graph()
g.parse(str(m4i))
g.parse(str(mapping))

outfile = str(ro_crate_context_file)
f = open(outfile, "w", encoding="utf-8")
f.write("{\n  \"@context\": {\n")

nm = NamespaceManager(g)

vocab_query = """
SELECT ?id 
WHERE {
?id rdf:type owl:Ontology .
}"""

query_result = g.query(vocab_query)

for row in query_result:
    f.write(f'    "@vocab": "{row.id}"')

for ns_prefix, namespace in g.namespaces():
    if ns_prefix.strip() != "":
        f.write(f',\n    "{ns_prefix}" : "{namespace}"')



        
for key, uri in predefined_entities.items():
    f.write(f',\n    "{key}" : "{uri}"')
    


properties = {}
classes = {}

query_mapping_properties: str = ('SELECT ?old ?new\n'
                  'WHERE {\n'
                  f'  ?old owl:equivalentProperty ?new .\n'
                  '}')
query_result = g.query(query_mapping_properties)


for row in query_result:
    properties[nm.normalizeUri(row.old)] = nm.normalizeUri(row.new)
    # print(nm.normalizeUri(row.old), " into ", nm.normalizeUri(row.new))


query_mapping_classes: str = ('SELECT ?old ?new\n'
                  'WHERE {\n'
                  f'  ?old owl:equivalentClass ?new .\n'
                  '}')
query_result = g.query(query_mapping_classes)


for row in query_result:
    classes[nm.normalizeUri(row.old)] = nm.normalizeUri(row.new)
    # print(nm.normalizeUri(row.old), " into ", nm.normalizeUri(row.new))


entities = [
    "owl:Class",
    "owl:AnnotationProperty",
    "rdfs:Datatype",
    "owl:ObjectProperty",
    "owl:DatatypeProperty",
    "owl:NamedIndividual"]

ids = []

for entity in entities:
    query: str = ('SELECT ?id ?label ?type\n'
                  'WHERE {\n'
                  f'  ?id rdf:type {entity} .\n'
                  '  ?id skos:prefLabel ?label.\n'
                  '  OPTIONAL {?id rdfs:range ?type .}. \n'
                  '}')
    print(f'*** Adding entities of type {entity} to the context file ***')
    query_result = g.query(query)

    for row in query_result:
        typestr = ""
        if type(row.type) == URIRef:
            typestr = f', "@type" : "{nm.normalizeUri(row.type)}"'
#            print(row.id, row.type, type(row.type))
        if row.label not in ids:
            id = nm.normalizeUri(row.id)
            idstr = classes[id] if id in classes else properties[id] if id in properties else id
            f.write(f',\n    "{row.label}" : {{"@id" : "{idstr}"{typestr}}}')
            ids.append(row.label.lower())
        else:
            print(f'"{row.label}" is already used as a label, therefore {row.id} will be skipped')




f.write("\n  }\n}")
f.close()


g.parse(outfile, format='json-ld')

assert g, f'Error: generated context file has syntax errors'


