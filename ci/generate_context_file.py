from rdflib import Graph
from rdflib.namespace import NamespaceManager
from rdflib.term import URIRef
from pathlib import Path

m4i = Path(__file__).parent.parent/'metadata4ing.ttl'
context_file = Path(__file__).parent.parent/'m4i_context_generated.jsonld'

g = Graph()
g.parse(str(m4i))

outfile = str(context_file)
f = open(outfile, "w", encoding="utf-8")
f.write("{\n  \"@context\": {\n")

nm = NamespaceManager(g)

vocab_query = """
SELECT ?id 
WHERE {
?id rdf:type owl:Ontology .
}"""

query_result = g.query(vocab_query)

for row in query_result:
    f.write(f'    "@vocab": "{row.id}"')

for ns_prefix, namespace in g.namespaces():
    if ns_prefix.strip() != "":
        f.write(f',\n    "{ns_prefix}" : "{namespace}"')


standard_entities = {
    "label" : "rdfs:label",
    "comment" : "rdfs:comment"    
}

ids = []

print(f'*** Adding standard entities to the context file ***')

for label, uri in standard_entities.items():
    if label not in ids:
        f.write(f',\n    "{label}" : {{"@id" : "{uri}"}}')
        ids.append(label)
    else:
        print(f'"{label}" is already used as a label, therefore {uri} will be skipped')
    

entities = [
    "owl:Class",
    "owl:AnnotationProperty",
    "rdfs:Datatype",
    "owl:ObjectProperty",
    "owl:DatatypeProperty",
    "owl:NamedIndividual"]


for entity in entities:
    query: str = ('SELECT ?id ?label ?type\n'
                  'WHERE {\n'
                  f'  ?id rdf:type {entity} .\n'
                  '  ?id skos:prefLabel ?label.\n'
                  '  OPTIONAL {?id rdfs:range ?type .}. \n'
                  '}')
    print(f'*** Adding entities of type {entity} to the context file ***')
    query_result = g.query(query)

    for row in query_result:
        typestr = ""
        if type(row.type) == URIRef:
            typestr = f', "@type" : "{nm.normalizeUri(row.type)}"'
#            print(row.id, row.type, type(row.type))
        if row.label not in ids:
            f.write(f',\n    "{row.label}" : {{"@id" : "{nm.normalizeUri(row.id)}"{typestr}}}')
            ids.append(row.label.lower())
        else:
            print(f'"{row.label}" is already used as a label, therefore {row.id} will be skipped')

f.write("\n  }\n}")
f.close()


g.parse(outfile, format='json-ld')

assert g, f'Error: generated context file has syntax errors'


