# Baking Bread (WIP) - An example application of m4i

This directory contains an application example draft for m4i which describes the process of baking bread.

Author: Ashish Karmacharya (@ashish.karmacharya)

## Import Hierarchy

[application_examples/bread-baking/BakingInstances.owl](application_examples/bread-baking/BakingInstances.owl) contains instances of baking processes and related instances of tools etc. It imports [application_examples/bread-baking/BakingOntology.owl](application_examples/bread-baking/BakingOntology.owl). This defines basic classes and properties to describe baking processes and associated parameters etc. The Baking Ontology itself imports Metadata4Ing 1.0.0 from <http://w3id.org/nfdi4ing/metadata4ing#>.

![Import Hierarchy](visualisations/Import_Hierarchy.PNG)


## Visualization of example

BakingInstances.owl contains data about baking processes occurrences. The information about these occurences of the activity 'baking' can be visualized as a graph:

![Information about three occurrences of baking](visualisations/instance-graph.png)